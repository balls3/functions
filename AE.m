classdef AE
    
    properties
    end
    
    methods (Static)
        function data = addMarks(data,line_name,time)
            
            marks=data.(line_name).AE.marks.times;
            marks(length(marks)+1)=time;
            marks=sort(marks);
            data.(line_name).AE.marks.times=marks;
            
        end
        
        function O =aicpicker(waveform,time,A,RV,samplerate)
            
            Z=waveform;
            z=abs(Z);
            l=length(Z);
            B = A/20;
            C=10^B;
            V = C*RV;
            l=1;
            
            
            timecp=time;
            
            a=mean(z);
            t=1;
            for w=1:l
                u =z(t:t+50);
                q = mean (u);
                if q >= 2*a
                    loop = 0;
                    m = t;
                    while loop == 0
                        I =z(m,1);
                        if I>=V
                            loop = 1;
                            cp = m;
                        else
                            loop =0;
                            m = m+1;
                        end
                    end
                else
                    t=t+1;
                    cp=0;
                end
            end
            if cp ==0
                clear I m cp q a u t;
                loop = 0;
                m = 1;
                while loop == 0
                    if m>length(z(:,1))
                        loop = 1;
                        cp = 1;
                    else
                        I =z(m,1);
                        if I>=V
                            loop = 1;
                            cp = m;
                        else
                            loop =0;
                            m = m+1;
                        end
                    end
                end
            end
            
            cpout=cp;
            N=cp+150;
            count=1;
            val=0;
            while val~=1
                if Z(count)== Z(count +1)
                    count = count +1;
                else
                    count=count+1;
                    val=1;
                end
            end
            j=1;
            for p=2:N-2
                if j < count + 5
                    AIC(j) = 1;
                else
                    F = var(Z(1:p,1),0,1);
                    K = var(Z(p+1:N,1),0,1);
                    AIC(j) = (j * log10(F)) + ((N - j - 1) * log10(K));
                    b = isinf(AIC(j));
                    if b ==1
                        AIC(j) = 1;
                    end
                end
                j = j+1;
            end
            Lr = find(AIC==min(AIC));
            
            
            if Lr>cpout
                clear L Lr AIC K F p b j;
                j=1;
                for p=2:N-2
                    if j < count + 5
                        AIC(j) = 1;
                    elseif j>cp
                        AIC(j) = 1;
                    else
                        F = var(Z(1:p,1),0,1);
                        K = var(Z(p+1:N,1),0,1);
                        AIC(j) = (j * log10(F)) + ((N - j - 1) * log10(K));
                        b = isinf(AIC(j));
                        if b ==1
                            AIC(j) = 1;
                        end
                    end
                    j = j+1;
                end
                Lr= find(AIC==min(AIC));
                locationr = Lr;
            else
                Lr  = find(AIC==min(AIC));
                locationr  = Lr;
            end
            
            location =locationr';
            AICtimes  = ((location-(cpout))*(1/samplerate))+timecp;
            AICtime = AICtimes';
            O=[AICtime];
            
            
        end
        
        function AICtimes=aicpickerDC(prelimAIC,prelimtimes,RV,samplerate,waveforms,threshold)
            
            start_times=zeros(length(waveforms(1,:)),1);
            
            for i=1:length(length(waveforms(1,:)))
                thresholdv = (10^(threshold/20))*RV;
                waveform=waveforms(:,i);
                crossing=find(waveform>thresholdv,1);
                start_times(i)=crossing/samplerate;
            end
            
            AICtimes=prelimtimes-(start_times+prelimAIC);
        end
        
        function[ratio]=ampRatio(data,threshold,fsamp)
            
            maxamp=max(data);
            
            a=1;
            
            for i=1:length(data)
                if data(i)>threshold
                    
                    thrlength(a)=i;
                    a=a+1;
                end
            end
            
            b=thrlength(end)-thrlength(1);
            btime=b/fsamp;
            
            ratio=btime/maxamp;
        end
        
        function[AE]=datawaveformcheck(AE)
            
            for i=1:length(AE.wf_times)
                wavesize=length(AE.wfs(:,i));
                threshold = 10^(AE.setups{1,AE.wf_chans(i)}.thr/20)*0.0001;
                count=0;
                j=1;
                first=0;
                last=0;
                [Amp,loc_Amp]=max(abs(AE.wfs(:,i)));
                
                while j<wavesize
                    if abs(AE.wfs(j,i))>threshold
                        if first == 0
                            first =j;
                        end
                        count=count+1;
                        j=j+1;
                        while j<wavesize && abs(AE.wfs(j,i))>threshold
                            j=j+1;
                        end
                        last=j-1;
                    end
                    j=j+1;
                end
                
                AE.AEredata(i,1)=AE.wf_times(i);                                        %time ID
                AE.AEredata(i,2)=AE.wf_chans(i);                                        %Channel number
                AE.AEredata(i,7) = round(20*log10(Amp/0.0001));                         %Amplitude
                AE.AEredata(i,4) = count;
                if count == 0 || (last-first)==0                                        %it will give NAN if either of these are the case
                    AE.AEredata(i,3) = 0;                                               %Ristitme
                    AE.AEredata(i,5) = 0;                                               %Energy within the time period that it is over the threshold
                    AE.AEredata(i,6) = 0;                                               %Duration
                else
                    AE.AEredata(i,3) = 1000000* (loc_Amp-first)/(1000*AE.setups{1,AE.wf_chans(i)}.srate);   %risetime in microseconds
                    AE.AEredata(i,5) = 10e17*(((sum(abs(AE.wfs(:,i))))))/((wavesize)*10000*1000*AE.setups{1,AE.wf_chans(i)}.srate);    %Not certain on this yet, its in attojoules but not 100% sure its correct as its possibly dependent on the system
                    AE.AEredata(i,6) = 1000000*(last-first) /(1000*AE.setups{1,AE.wf_chans(i)}.srate);      %Duration in microseconds
                end
            end
        
        end
    
        function [dur]=duration(x,y,z)
            AbsValues=abs(x);
            i=1;
            j=1;
            durmatrix=zeros(length(AbsValues),1);
            for i=1:length(AbsValues)
                if AbsValues(i)>z
                    durmatrix(j)=i;
                    j=j+1;
                end
                i=i+1;
            end
            dur=(max(durmatrix)-min(durmatrix))/y;
        end
        
        function [ene]=energy(x)
            i=1;
            Pos=zeros(1,length(x));
            Neg=zeros(1,length(x));
            for i=1:length(x)
                if x(i)<0
                    Pos(i)=0;
                    Neg(i)=x(i);
                elseif x(i)>0
                    Pos(i)=x(i);
                    Neg(i)=0;
                else
                    Pos(i)=0;
                    Neg(i)=0;
                end
                i=i+1;
            end
            ene=trapz(Pos)+trapz(abs(Neg));
        end

        function x = euclidean(locations,position)
            
            euclid=zeros(length(locations(:,1)),1);
            
            for i=1:length(locations(:,1))
                euclid(i)=sqrt((locations(i,1)-position(1))^2+(locations(i,2)-position(2))^2);
            end
            
            x=mean(euclid);
        end
        
        function[proportionx,lengthx,startx,histox] = eventLength(data,windowlength,threshold)
            
            
            a=0;
            b=length(data)/windowlength;
            c=round(b+1);
            data2=zeros(c*windowlength,1);
            data2(1:length(data))=data;
            histox=zeros(c,1);
            
            for i=1:length(data)
                if data(i)>threshold
                    a=a+1;
                end
            end
            
            proportionx=a/length(data);
            
            data3=abs(data2);
            
            lengthx=0;
            startx=0;
            
            for i=1:c
                e=0;
                for j=1:windowlength-1
                    d=data3((i*windowlength)-j);
                    if d>threshold
                        e=e+1;
                    end
                end
                histox(i)=e;
                if e>0
                    lengthx=lengthx+1;
                end
                if startx==0 && e>0
                    startx=i;
                end
                
            end
        end
        
        function graph = plotEvent(waveform,fsamp,graph_name,x_label,y_label,ylimits)
            
            time=samp2time(waveform,fsamp);
            figure
            graph=plot(time,waveform);
            hold on
            title(graph_name)
            xlabel(x_label)
            ylabel(y_label)
            grid on
            axis([0 max(time) (ylimits)])
        end
        
        function [ris] = riseTime(x,y,z)
            AbsValues=abs(x);
            i=1;
            j=1;
            rismatrix=zeros(length(AbsValues),1);
            for i=1:length(AbsValues)
                if AbsValues(i)>z && AbsValues(i)<max(x)
                    rismatrix(j)=i;
                    j=j+1;
                elseif AbsValues(i)>=max(x)
                    
                    break
                end
                i=i+1;
            end
            ris=(max(rismatrix)-min(rismatrix))/y;
        end
        
        function timeaxis = samp2time(waveform,fsamp)
            
            duration=length(waveform)/fsamp;
            timeaxis=linspace(0,duration,length(waveform));
        end
        
        function waveform = v2db(waveform,RV)
            
            if isempty(RV)==1
                RV=0.0001;
            end
            
            waveform2=zeros(length(waveform),1);
            
            for i=1:length(waveform)
                if waveform(i)==0
                    waveform2(i)=0;
                else
                    waveform2(i)=20*log10(abs(waveform(i))/RV);
                end
                if waveform(i)<0
                    waveform2(i)=waveform2(i)*(-1);
                end
            end
            waveform=waveform2;
        end
        
        function coords = intercept(xy1,xy2)
            coords(1)=(xy1(2)-xy2(2))/((xy1(2)/xy1(1))-(xy2(2)/xy2(1)));
        end
        
        function dominantFrequency = domFreq(Y,Fs)
            
            L=length(Y);
            P2=abs(Y/L);
            P1=P2(1:L/2+1);
            P1(2:end-1)=2*P1(2:end-1);
            f=Fs*(0:(L/2))/L;
            [~,idx]=max(P1);
            dominantFrequency=f(idx);
            
        end
            
        function gridData = vibConvert(sortedData)
            x=sortedData.parameters.x;
            y=sortedData.parameters.y;
            
            [pks,locs,w,p] = findpeaks(diff(x));
            noColumns=find(p>mean(p));
            noColumns=locs(noColumns);
            xGrid=zeros(1,length(noColumns));
            yGrid=zeros(1,length(noColumns));
            for i = 1:length(noColuns)
                if i==1
                    xGrid(:,i)=x(1:(noColumns(i)-1));
                    yGrid(:,i)=y(1:(noColumns(i)-1));
                else
                    xGrid(:,i)=x(noColumns(i):(noColumns(i+1)-1));
                    yGrid(:,i)=y(noColumns(i):(noColumns(i+1)-1));
                end
            end
            
            [xq,yq] = meshgrid(linspace(0.0025,0.25,100), linspace(0.0025,0.25,100));
            gridData=zeros(100,100,1024);
            for j=1:1024
                v=zeros(length(x),1);
                for i=1:length(x)
                v(i)=sortedData.waveforms{i}(j);
                end
                vq = griddata(x,y,v,xq,yq);
                gridData(:,:,j)=vq;
            end
        end
    end
end