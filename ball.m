classdef ball
    
    properties
    end
    
    methods (Static)
        function timeaxis=samp2time(waveform,fsamp)
            
            duration=length(waveform)/fsamp;
            timeaxis=linspace(0,duration,length(waveform));
        end
        
        function waveform = v2db(waveform,RV)
            
            if isempty(RV)==1
                RV=0.0001;
            end
            
            waveform2=zeros(length(waveform),1);
            
            for i=1:length(waveform)
                if waveform(i)==0
                    waveform2(i)=0;
                else
                    waveform2(i)=20*log10(abs(waveform(i))/RV);
                end
                if waveform(i)<0
                    waveform2(i)=waveform2(i)*(-1);
                end
            end
            waveform=waveform2;
        end
        
    end
    
end